package cn.blogsx.service.impl;

import cn.blogsx.service.HelloService;
import com.alibaba.dubbo.config.annotation.Service;

@Service //此处注意要使用dubbo提供的@Service注解
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String name) {
        return "Hello + :" + name;
    }
}
